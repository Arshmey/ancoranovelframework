﻿using System;
using System.Threading;

namespace AncoraNovelFramework.StoryTallerV2
{
    public class NewTextPrint
    {
        public int speedText { get; set; }
        public int pauseText { get; set; }
        NewCommand command = new NewCommand();

        public NewTextPrint()
        {
            Console.ForegroundColor = ConsoleColor.White;
            speedText = 0;
            pauseText = 1;
        }
        public void PrintText(String Text, NewAction newAction)
        {
            command.Setup(Text);
            for (int i = 0; i < command.Text.Length; i++)
            {
                command.CheckCommand(i);
                newAction.WhatIsAction(command, i, this);
                if (newAction.isWrote != true)
                {
                    if (speedText < 1) { Console.Write(command.Text[i]); Thread.Sleep(pauseText); }
                    else
                    {
                        foreach (char chr in command.Text[i]) { Console.Write(chr); Thread.Sleep(speedText); }
                    }
                    if (!command.End)
                    {
                        command.End = true;
                        Console.WriteLine();
                        Console.Clear();
                    }
                }
            }
            Thread.Sleep(pauseText / 2);
            Console.WriteLine();
            newAction.isWrote = false;
        }

        public void PrintText(String Text)
        {
            command.Setup(Text);
            for (int i = 0; i < command.Text.Length; i++)
            {
                command.CheckCommand(i);
                if (speedText < 1) { Console.Write(command.Text[i]); Thread.Sleep(pauseText); }
                else
                {
                    foreach (char chr in command.Text[i]) { Console.Write(chr); Thread.Sleep(speedText); }
                }
                if (!command.End)
                {
                    command.End = true;
                    Console.ReadLine();
                    Console.Clear();
                }
            }
            Thread.Sleep(pauseText / 2);
            Console.WriteLine();
        }
    }
}