﻿using System;
using System.Collections.Generic;

namespace AncoraNovelFramework.StoryTallerV2
{
    public class NewCommand
    {
        public String[] Text { get; set; }
        public bool End { get; set; }
        public String Key { get; set; }
        public NewCommand() { End = true; Key = ""; }
        private int textLenght = 0;

        public void Setup(String Text)
        {
            this.Text = Text.Split('|');
        }
        public void CheckCommand(int ID)
        {
            while (true)
            {
                if (Text[ID].Contains("(color)")) { SetupColor(ID); }
                else if (Text[ID].Contains("(white)")) { EarseCommandOnText("(white)", ID); White(); }
                else if (Text[ID].Contains("(center)")) { EarseCommandOnText("(center)", ID); Center(Text[ID]); }
                else if (Text[ID].Contains("(!n)")) { NewLine(ID); }
                else if (Text[ID].Contains("(end)")) { EarseCommandOnText("(end)", ID); EndAct(); }
                else if (Text[ID].Contains("(action)")) { Key = Text[ID].Replace("((action)", ""); EarseCommandOnText("((action)", ID); }
                else { break; }
            }
        }
        private void SetupColor(int ID)
        {
            if (Text[ID].Contains("((color)white)")) { EarseCommandOnText("((color)white)", ID); Console.ForegroundColor = ConsoleColor.White; }
            else if(Text[ID].Contains("((color)red)")) { EarseCommandOnText("((color)red)", ID); Console.ForegroundColor = ConsoleColor.Red; }
            else if(Text[ID].Contains("((color)yellow)")) { EarseCommandOnText("((color)yellow)", ID); Console.ForegroundColor = ConsoleColor.Yellow; }
            else if(Text[ID].Contains("((color)green)")) { EarseCommandOnText("((color)green)", ID); Console.ForegroundColor = ConsoleColor.Green; }
            else if(Text[ID].Contains("((color)blue)")) { EarseCommandOnText("((color)blue)", ID); Console.ForegroundColor = ConsoleColor.Blue; }
            else if(Text[ID].Contains("((color)purple)")) { EarseCommandOnText("((color)purple)", ID); Console.ForegroundColor = ConsoleColor.Magenta; }
        }
        private void White()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        private void Center(String line)
        {
            textLenght = line.Length;
            Filtration(line);
            Console.SetCursorPosition((Console.WindowWidth - textLenght) / 2, Console.CursorTop);
        }
        private void Filtration(String line)
        {
            while (true)
            {
                if (line.Contains("(red)")) { textLenght -= 5; line = line.Replace("(red)", ""); }
                else if (line.Contains("(white)")) { textLenght -= 7; line = line.Replace("(white)", ""); }
                else if (line.Contains("(center)")) { textLenght -= 8; line = line.Replace("(center)", ""); }
                else if (line.Contains("(!n)")) { textLenght -= 4; line = line.Replace("(!n)", ""); }
                else if (line.Contains("(end)")) { textLenght -= 5; line = line.Replace("(end)", ""); }
                else { break; }
            }
        }
        private void NewLine(int ID)
        {
            Text[ID] = Text[ID].Replace("(!n)", "\n");
        }
        private void EndAct() { End = false; }
        private void EarseCommandOnText(String COMMAND, int ID)
        {
            Text[ID] = Text[ID].Replace(COMMAND, "");
        }
    }
}
