﻿using System;
using System.Collections.Generic;

namespace AncoraNovelFramework.StoryTallerV2
{
    public class NewAction
    {
        private Dictionary<String, String> ActionList = new Dictionary<String, String>();
        public bool isWrote { get; set; }
        public NewAction(Dictionary<String, String> StorageAction) { ActionList = StorageAction; isWrote = false; }
        private String SpecialActionKey;

        public void WhatIsAction(NewCommand command, int Pos, NewTextPrint newTextPrint)
        {
            if(command.Key.Contains("_output)")) { Output(command, Pos); Earse(command); }
            else if(command.Key.Contains("_write)")) { Write(command, Pos); Earse(command); }
            else if(command.Key.Contains("_choose)")) { Choose(command, Pos, newTextPrint); Earse(command); }
            else if(command.Key.Contains("_reaction)")) { Reaction(command, Pos, newTextPrint); Earse(command); }
        }
        private void Output(NewCommand command, int Pos)
        {
            command.Text[Pos] = command.Key = command.Text[Pos].Replace("_output)", "");
            command.Text[Pos] = command.Text[Pos].Replace(command.Key, ActionList[command.Key]);
        }
        private void Write(NewCommand command, int Pos)
        {
            command.Text[Pos] = command.Key = command.Text[Pos].Replace("_write)", "");
            command.Text[Pos] = command.Text[Pos].Replace(command.Key, "");
            SpecialActionKey = Console.ReadLine(); Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
            ActionList[command.Key] = ActionList[command.Key].Replace(ActionList[command.Key], SpecialActionKey);
        }
        private void Choose(NewCommand command, int Pos, NewTextPrint newTextPrint)
        {
            command.Text[Pos] = command.Key = command.Text[Pos].Replace("_choose)", "");
            command.Text[Pos] = command.Text[Pos].Replace(command.Key, "");
            newTextPrint.PrintText(ActionList[command.Key]);
            Console.Write("Choose>: ");
            SpecialActionKey = Console.ReadLine(); Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
            isWrote = true;
        }
        private void Reaction(NewCommand command, int Pos, NewTextPrint newTextPrint)
        {
            command.Text[Pos] = command.Key = command.Text[Pos].Replace("_reaction)", "");
            if(ActionList.ContainsKey(command.Key + SpecialActionKey)) {
                command.Text[Pos] = command.Text[Pos].Replace(command.Key, ActionList[command.Key + SpecialActionKey]);
            } else { command.Text[Pos] = command.Text[Pos].Replace(command.Key, ActionList[command.Key]); }
        }
        private void Earse(NewCommand command)
        {
            command.Key = "";
        }
    }
}
