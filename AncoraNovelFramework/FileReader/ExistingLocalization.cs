﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AncoraNovelFramework.FileReader
{
    public class ExistingLocalization
    {

        private Dictionary<int, String> Languages = new Dictionary<int, String>();

        public ExistingLocalization()
        {
            foreach (string line in System.IO.File.ReadLines(@"Localization\LocalizationFiles.txt"))
            {
                String[] temp = line.Split(new string[] { ">:" }, StringSplitOptions.None);
                Languages.Add(int.Parse(temp[0]), temp[1].Trim()); 
            }
        }

        public String getLanguages(int key)
        {
            return Languages[key];
        }

        public int getCount()
        {
            return Languages.Count();
        }
    }
}
