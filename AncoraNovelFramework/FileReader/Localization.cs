﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace AncoraNovelFramework.FileReader
{
    public class Localization
    {
        private int i = 0;
        private Dictionary<int, String> Speaker = new Dictionary<int, String>();
        private Dictionary<String, String> speakText = new Dictionary<String, String>();
        private Dictionary<String, String> systemText = new Dictionary<String, String>();
        public String Folder { get; set; } 

        public void TextTranslation(String folder_loc, int num)
        {
            Folder = folder_loc;
            foreach (string line in System.IO.File.ReadLines(@"Localization\" + folder_loc + "\\Act"+ num + ".txt"))
            {
                String[] temp = line.Split(new string[] { ">:" }, StringSplitOptions.None);
                speakText.Add(temp[0], temp[1].Trim());
                Speaker.Add(i++, temp[0]);
            }
        }

        public void SystemTranslation(String folder_loc)
        {
            foreach (string line in System.IO.File.ReadLines(@"Localization\" + folder_loc + "\\System.txt"))
            {
                String[] temp = line.Split(new string[] { ">:" }, StringSplitOptions.None);
                systemText.Add(temp[0], temp[1].Trim());
            }
        }
        public String getSystemTranslation(String key)
        {
            return systemText[key];
        }

        public String getSpeakText(String key)
        {
            return speakText[key];
        }
        public String getSpeaker(int key)
        {
            return Speaker[key];
        }

        public int getSpeakerCount()
        {
            return speakText.Count;
        }
    }
}
