﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncoraNovelFramework.FileReader
{
    public class ActionReader
    {
        private Dictionary<String, String> StorageAction = new Dictionary<String, String>();

        public void ActionReaderFile(String folder_loc)
        {
            foreach (string line in System.IO.File.ReadLines(@"Localization\" + folder_loc + "\\Action.txt"))
            {
                String[] temp = line.Split(new string[] { ">:" }, StringSplitOptions.None);
                StorageAction.Add(temp[0], temp[1].Trim());
            }
        }
        public String getStorageAction(String key)
        {
            return StorageAction[key];
        }
        public Dictionary<String, String> getStorageAction()
        {
            return StorageAction;
        }
    }
}
