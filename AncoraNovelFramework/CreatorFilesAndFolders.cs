﻿using System;
using System.IO;

namespace AncoraNovelFramework
{
    public class CreatorFilesAndFolders
    {

        public void CreateStandartLocalization()
        {
            if (!Directory.Exists(@"Localization\")) { Directory.CreateDirectory(@"Localization\"); }
            if (!File.Exists(@"Localization\LocalizationFiles.txt")) { File.WriteAllText(@"Localization\LocalizationFiles.txt", "1>:Russian\n2>:English"); }
            if (!Directory.Exists(@"Localization\Russian\")) { Directory.CreateDirectory(@"Localization\Russian\"); }
            if (!File.Exists(@"Localization\Russian\Act0.txt")) { File.Create(@"Localization\Russian\Act0.txt"); }
            if (!File.Exists(@"Localization\Russian\System.txt")) { File.Create(@"Localization\Russian\System.txt"); }
            if (!File.Exists(@"Localization\Russian\Action.txt")) { File.Create(@"Localization\Russian\Action.txt"); }
            if (!Directory.Exists(@"Localization\English\")) { Directory.CreateDirectory(@"Localization\English\"); }
            if (!File.Exists(@"Localization\English\Act0.txt")) { File.Create(@"Localization\English\Act0.txt"); }
            if (!File.Exists(@"Localization\English\System.txt")) { File.Create(@"Localization\English\System.txt"); }
            if (!File.Exists(@"Localization\English\Action.txt")) { File.Create(@"Localization\English\Action.txt"); }
        }
        public void CreateMusic()
        {
            if (!Directory.Exists(@"Music\")) { Directory.CreateDirectory(@"Music\"); }
        }

    }
}
