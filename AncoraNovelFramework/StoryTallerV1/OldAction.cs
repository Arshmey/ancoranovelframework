﻿using System;
using System.Collections.Generic;

namespace AncoraNovelFramework.StoryTallerV1
{
    internal class OldAction
    {
        private String Wrote = "";
        private String Chose = "";
        
        public void WhatIsAction(OldCommand command, Dictionary<String, String> StorageAction, OldTextPrint print, int pos)
        {
            if(command.Save.Contains("_write")) { WriteAction(StorageAction, command, print); }
            else if (command.Save.Contains("_choose")) { ChooseAction(StorageAction, command, print); }
            else if(command.Save.Contains("_reaction")) { Reaction(StorageAction, command, pos); }
            else { }
        }
        private void WriteAction(Dictionary<String, String> StorageAction, OldCommand command, OldTextPrint print)
        {
            print.ActionPrint(StorageAction[command.Save.Replace("_write)", "")]);
            command.Save = "";
            Wrote = Console.ReadLine();
        }
        private void ChooseAction(Dictionary<String, String> StorageAction, OldCommand command, OldTextPrint print)
        {
            command.Save = command.Save.Replace("_choose)", ""); int Count = 1;
            for(int i = 0; i < StorageAction.Count; i++) {  if(StorageAction.ContainsKey(command.Save + Count)) 
                { print.ActionPrint(StorageAction[command.Save + Count]); Count++; } }
            Console.Write("You are shoosing: ");
            Chose = Console.ReadLine();
    }
        private void Reaction(Dictionary<String, String> StorageAction, OldCommand command, int pos)
        {
            if (Wrote != "")
            {
                command.Save = command.Save.Replace("_reaction)", "");
                command.Text[pos] = StorageAction[command.Save].Replace(StorageAction[command.Save], Wrote);
                command.Save = ""; Wrote = "";
            } 
            else if (Chose != "")
            {
                command.Save = command.Save.Replace("_reaction)", "");
                command.Text[pos] = StorageAction[command.Save + Chose];
                command.Save = ""; Chose = "";
            } else { }
        }
    }
}
