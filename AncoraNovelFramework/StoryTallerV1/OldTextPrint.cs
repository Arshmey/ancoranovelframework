﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace AncoraNovelFramework.StoryTallerV1
{
    public class OldTextPrint
    {
        public int speedText { get; set; }
        public int pauseText { get; set; }
        OldCommand command = new OldCommand();
        OldAction action;

        public OldTextPrint()
        {
            Console.ForegroundColor = ConsoleColor.White;
            speedText = 0;
            pauseText = 1;
            action = new OldAction();
        }

        public void PrintText(String Text, Dictionary<String, String> StorageAction)
        {
            command.Setup(Text);
            for (int i = 0; i < command.Text.Length; i++)
            {
                command.CheckCommand(i);
                action.WhatIsAction(command, StorageAction, this, i);
                if (speedText < 1) { Console.Write(command.Text[i]); Thread.Sleep(pauseText); }
                else
                {
                    foreach (char chr in command.Text[i]) { Console.Write(chr); Thread.Sleep(speedText); }
                }
                if (!command.End)
                {
                    command.End = true;
                    Console.ReadLine();
                    Console.Clear();
                }
            }
            Thread.Sleep(pauseText / 2);
            Console.WriteLine();
        }
        public void PrintText(String Text)
        {
            command.Setup(Text);
            for (int i = 0; i < command.Text.Length; i++)
            {
                command.CheckCommand(i);
                if (speedText < 1) { Console.Write(command.Text[i]); Thread.Sleep(pauseText); }
                else
                {
                    foreach (char chr in command.Text[i]) { Console.Write(chr); Thread.Sleep(speedText); }
                }
                if (!command.End)
                {
                    command.End = true;
                    Console.ReadLine();
                    Console.Clear();
                }
            }
            Thread.Sleep(pauseText / 2);
            Console.WriteLine();
        }

        public void ActionPrint(String Text)
        {
            foreach (char chr in Text) { Console.Write(chr); Thread.Sleep(45); }
            Console.WriteLine();
        }
    }
}