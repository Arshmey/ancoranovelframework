﻿using System;
using WMPLib;

namespace AncoraNovelFramework.Music
{
    public class GameMusicPlayer
    {

        WindowsMediaPlayer windowsMedia = new WindowsMediaPlayer();

        public void MusicLoad(String Sound)
        { 
            windowsMedia.URL = @"Music\" + Sound;
        }

        public void StartPlayingMusic() 
        {
            windowsMedia.controls.play();
        }

        public void StopPlayingMusic()
        {
            windowsMedia.controls.stop();
        }

        public void StartPlayLoop()
        {
            windowsMedia.settings.setMode("loop", true);
            windowsMedia.controls.play();
        }

        public void SetVolume(int Volume)
        {
            windowsMedia.settings.volume = Volume;
        }

    }
}
